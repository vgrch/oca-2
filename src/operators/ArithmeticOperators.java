package operators;

public class ArithmeticOperators {

    // you can use any arithmetic operator with boolean variables
    public static void booleanArithmetic() {
        boolean b1 = true, b2 = false;
        // System.out.println(b1+b2); // compile error
    }


    // IMPLICIT WIDENING OF DATA TYPES IN AN ARITHMETIC OPERATION
    // For  arithmetic  operations  with  data  types char, byte, short,  or int,  all  operand  values  are  widened  to int.
    // If  an  arithmetic  operation includes  the  data  type long,  all  operand  values  are  widened  to long.
    // If  an arithmetic operation includes a data type of float or double, all operand val-ues are widened to double.
    public static void implicit_widening() {

        byte b1 = 1;
        byte b2 = 2;
        // fail to compile
        // short sum = b1 + b2;

        final byte b3 = 3;
        final byte b4 = 4;

        short sum1 = b3 + b4; // will compile successfully
    }

    // unary increment and decrement operators
    public static void unary() {

        int i = 5;
        // postfix notation
        System.out.println(i++); // will print 5 then i will 6
        i = 10;
        // prefix notation
        System.out.println(--i); // will print 9 then i will 9

    }

    public static void main(String[] args) {
        unary();
    }
}