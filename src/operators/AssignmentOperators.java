package operators;

public class AssignmentOperators {

//    a -= b is equal to a = a – b
//    a += b is equal to a = a + b
//    a *= b is equal to a = a * b
//    a /= b is equal to a = a / b
//    a %= b is equal to a = a % b

    public static void main(String[] args) {
        int a = 90, b = 10;
        a += b; // a = a + b;
        System.out.println(a); // 100
        a -= b; // a = a - b
        System.out.println(a); // 90
        assignDifferentDatatypes();
    }

    // assign different datatypes
    public static void assignDifferentDatatypes() {
        // you cannot assign wide range datatype variable to smaller ranged datatype variable, it cause compile error
        int a = 100;
        long b = 247826594189l;
        // a = b; // compile error
        // but you can assign with casting
        //  In that case, the compiler proceeds by chopping off any extra bits that may not fit into the smaller variable, but it will cause unexpected results
        a = (int) b;
        System.out.println(b); // 247826594189
        System.out.println(a); // -1281508979
        // An int can easily fit into a long
        a = 10;
        b = a;
        System.out.println(b);
    }

    public static void assignmentWithDeclaration() {

        boolean b1 = false, b2, b3, b4;
        b1 = b2 = b3 = b4 = true;

        int a = 10 , b = 11;

       //  int i = d = 10; //wrong declaration with assignment
    }

}
