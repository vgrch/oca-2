package primitivevariables;

public class PrimitiveTypes {

    // non numeric primitive data types
    boolean _boolean;

    // numeric primitive data types

    // unsigned
    char _char;

    //signed
    // float point numbers
    double _double;

    float _float;

    // integer numbers
    byte _byte;

    short _short;

    int _int;

    long _long;

}
