package primitivevariables;

public class PrimitiveTypesMemoryAllocation {

    /*
    1 byte = 8 bits = 2^8
    |1| |1| |1| |1| |1| |1| |1| |1| its 8 bits binary
    1/0  64  32  16  8   4   2   1 decimal representation is 127
    if MSB is 1 number is negative and vice versa
    first bit is most significant bit define type(negative or positive) of number
     */
    // decimal range -128 : 127
    static byte _byte_max = 127;
    static byte _byte_min = -128;
    static byte _byte_zero = 0;

    public static void test_byte(){

        _byte_max = 0b0111_1111; // same as 127 integer
        System.out.println("Max value of byte as binary: "+Integer.toString(_byte_max,2) + " as integer: " + _byte_max);
        // will print Max value of byte as binary: 1111111 as integer: 127
        _byte_zero = 0b0000_0000; // same as 0 integer
        System.out.println("Zero value of byte as binary: "+Integer.toString(_byte_zero,2) + " as integer: " + _byte_zero);
        // will print Zero value of byte as binary: 0 as integer: 0
        _byte_min = -0b1000_0000;  // same as -128 integer
        System.out.println("Min value of byte as binary: "+Integer.toString(_byte_min,2) + " as integer: " + _byte_min);
        // will print Min value of byte as binary: -10000000 as integer: -128
    }


    /*
    short = 2 bytes
    |1|   |1|   |1|   |1|   |1|  |1|  |1| |1|   |1| |1| |1| |1| |1| |1| |1| |1| // its 2 bytes 16 bits
    1/0   16384 8192  4096  2048 1024 512 256   128 64  32  16   8   4   2   1  // decimal representation is 32767
     */

    //range -32768 : 32767
    static short _short_max = 32767;
    static short _short_zero = 0;
    static short _short_min = -32768;


    public static void test_short(){

        _short_max = 0b0111_1111_1111_1111; // same as 32767 as integer
        System.out.println("Max value of short as binary: " + Integer.toString(_short_max,2) + " as integer: " + _short_max);
        // Max value of short as binary: 111111111111111 as integer: 32767
        _short_max = 0b0000_0000_0000_0000; // same as 0 as integer
        System.out.println("Zero value of short as binary: " + Integer.toString(_short_max,2) + " as integer: " + _short_max);
        // Zero value of short as binary: 0 as integer: 0
        _short_min = -0b1000_0000_0000_0000; // compile error why ?
        System.out.println("Min value of short as binary: " + Integer.toString(_short_min,2) + " as integer: " + _short_min);
        // Min value of short as binary: -1000 0000 0000 0000 as integer: -32768
    }

        /*
    int = 2 shorts = 4 bytes = 32 bits
    2^(32-1)-1
     */

    /*
    long = 2 int = 4 short = 8 bytes = 64 bits
     */

    public void test_all(){
        byte _byte;
        short _short;
        int _int;
        long _long;
        float _float;
        double _double;

        char _char = 122;


        _byte = 127;
        _byte = 0b0111_1111;
        _byte = 01_23;
        _byte = 0x1_F;
        // _byte = 128;
        // _byte = 123l;
        // _byte = 1.1;

        _short = 32767;
        _short = 32_76__7;
//        _short = 123l;
//        _short = 327.67;

        _int = 2_147_483_647;
        //_int = 2_147_483_648;
        //_int = 123.21;
        //_int = 1000l;

        _long = 9_223_372_036_854_775_807l;
        _long = 2_147_483_647; // you can assign int value to long variable
        _long = _int + 1l ;
        _long = 2_147_483_648l; // if variable out of range int which is long range you must define L or l end of literal

        //_long = 123.21;

        _float = 1F;
        _float = 1;
        _float = 1L;
        _float = 1.1f;
        _float = _int;
        _float = _long;
        _float = _short;
        _float = _byte;
        _float = _char;
        _float = _int + _long;
        //_float = 1.2d;
        //_float = 1.3;

        _double = 1;
        _double = 1l;
        _double = 1.1;
        _double = 1.2f;

        _double = _int;
        _double = _long;
        _double = _short;
        _double = _byte;
        _double = _char;

        _double = _float + 1l +0.5;
        _double = 1.0d;
    }



    public static void main(String[] args) {
    }
}
