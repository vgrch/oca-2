//package primitivevariables;
//
//public class UnderscoreWithPrimitiveLiterals {
//
//    // java 7 introduced the use of underscore as part of literal values for make more-readable literal values
//    public static void underscore_with_literal_values() {
//
//        int _binary = 0b101010101;       // 0b prefix indicate this value is binary, decimal representation is 341
//        int _octal = 05732;              // 0 prefix indicates this value is octal, decimal representation is 3034
//        int _decimal = 123456789;        // decimal
//        int _hexadecimal = 0xa10365E;    // 0x prefix indicates this value is hexadecimal, decimal representation is 252720734
//
//        // you can place an underscore right after octal indicator but cannot after other indicators;
//        _octal = 0_123_456;
//        _binary = 0b_101010101; // incorrect, will cause compile error
//        _hexadecimal = 0x_F10365E; // incorrect, will cause compile error
//
//        // can not start or end literal value with an underscore
//        _decimal = _123456;
//        _decimal = 123456_;
//
//        // can not place underscore prior L,l,F,f,D,d and e(scientific notation ) suffixes , and after or prior decimal point
//        float _float = 123.456_f; // incorrect
//        _float = 123._456f; // incorrect
//        _float = 123_456.678_9f; // correct
//        long _long = 123_456_789_l; // incorrect
//        _long = 123_456_789;  // correct
//        double _scientific = 1.20__1762e2; // correct
//        _scientific = 1.20__1762e2; // correct
//        _scientific = 1.201762_e2; // incorrect
//
//        // there is not any problem with multiple underscore
//        _decimal = 123___345;
//
//        // below line will compile successfully but fail at runtime. you cannot use underscore where string digits expected
//        int i = Integer.parseInt("45_98");
//    }
//}