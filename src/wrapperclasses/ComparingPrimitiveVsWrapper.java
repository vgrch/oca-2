package wrapperclasses;

public class ComparingPrimitiveVsWrapper {

    //comparison different primitive
    public void compare_primitive_types() {
        short _short = 12;
        int _int = 12;
        System.out.println(_int == _short);
    }

    //comparison different wrapper classes
    public static void compare_wrapper() {
        Short _short = 12;
        Integer _int = 12;
        System.out.println("wrapper comparison");
        //System.out.println(_int == _short); // will not compile
    }

    // compare primitive with wrapper of same type
    static void compare_prim_vs_wrapper() {
        Integer _integer = Integer.valueOf(130);
        int _int = 130;

        // public int compareTo(Integer anotherInteger)
        // compareTo method accept Integer type as argument, when you try to pass int primitive type as argument
        // autoboxing executes, autoboxing uses .valueOf() static method. as you know static method accessible using null reference variable
        System.out.println(_integer.compareTo(Integer.valueOf(_int)));

        System.out.println(_integer.compareTo(_int));

        // _integer variable hold object Integer.valueOf(130) in heap memory
        // how this calculation available ?
        // its unboxing this situation _integer call its _integer.intValue() and compare 2 primitive types
        System.out.println(_int + _integer.intValue());

        System.out.println(_int + _integer);

    }

    public static void main(String[] args) {
        compare_prim_vs_wrapper();
    }

}