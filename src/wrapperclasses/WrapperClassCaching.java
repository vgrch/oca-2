package wrapperclasses;

public class WrapperClassCaching {

    // The valueOf()  method  returns  an  object  of  the  corresponding  wrapper  class
    // Wrapper  classes Byte, Short, Integer, Long(-128 - 127) and Character(0 - 127) cache objects
    // Wrapper  classes Float  and Double  don’t  cache  objects  for  anyrange of values.

    static void object_caching() {

        Integer integer = Integer.valueOf(127);
        Integer integer1 = Integer.valueOf(127);
        System.out.println(integer == integer1); // true; it means this reference variables refer same object in memory

        //   Byte, Short, Integer, Long in range (-128 - 127)
        Integer integer2 = Integer.valueOf(128);
        Integer integer3 = Integer.valueOf(128);
        System.out.println(integer2 == integer3); // false; 128 is out of caching range, integer2 and integer3 refer different objects in memory

        Integer integer4 = new Integer(127);
        Integer integer5 = new Integer(127);
        System.out.println(integer4 == integer5); // despite 127 is in range of caching new keyword create new instance of Integer class in memory

        // Autoboxing returns a cached copy within range (-128 - 127)
        Integer integer6 = 127;
        Integer integer7 = 127;
        System.out.println(integer6 == integer7);
        System.out.println(integer6 == integer1);

        // Autoboxing returns a cached copy within range (-128 - 127)
        Integer integer8 = 128;
        Integer integer9 = 128;
        System.out.println(integer8 == integer9);
    }

    static void boolean_caching() {

        // when Boolean class been load memory constant assign new Boolean object
        //     public static final Boolean FALSE = new Boolean(false);
        //     public static final Boolean TRUE = new Boolean(true);

        System.out.println("");
        Boolean boolean1 = new Boolean(true);
        Boolean boolean2 = new Boolean(true);
        System.out.println(boolean1 == boolean2); // false


        //         return (b ? TRUE : FALSE);
        Boolean boolean3 = Boolean.valueOf(false);
        Boolean boolean4 = Boolean.valueOf("false");
        System.out.println(boolean3 == boolean4); // true refers same object with caching

        // return memory address that constants refer
        Boolean boolean5 = Boolean.FALSE;
        Boolean boolean6 = Boolean.FALSE;
        System.out.println(boolean5 == boolean6);  // true

        // autoboxing
        // Autoboxing returns a cached copy
        Boolean boolean7 = true;
        Boolean boolean8 = true;
        System.out.println(boolean7 == boolean8);  // true
    }




    public static void main(String[] args) {
        object_caching();
        boolean_caching();
    }
}
