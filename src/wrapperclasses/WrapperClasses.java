package wrapperclasses;

public class WrapperClasses {

//    Java  defines  a  wrapper  class  for  each  of  its  primitive  data  types.
//    The  wrapper  classes are used to wrap primitives in an object, so they can be added to a collection object.

    //    Creating objects of the wrapper classes
    public static void retrieve_instance(){
        //    Assignment — By assigning a primitive to a wrapper class variable (autoboxing)
        Integer _int = 0_576;
        // reverse of autoboxing is unboxing
        int i = new Integer(5);

        //    Constructor — By using wrapper class constructors
        //    neither of these classes defines a default no-argument  constructor.
        //    All  wrapper  classes  (except Character)  define  a  constructor  that accepts a String argument.
        Integer $int = new Integer("5");
        Integer $anotherint = new Integer(5);

        //    Static methods — By calling static method of wrapper classes, like, valueOf()
        Integer int1 = Integer.valueOf("15");
    }


    // retrieving primitive values from the wrapper classes
    public static void retieve_primitive() {

        // primitiveValue() used for retrieving wrapper object primitive value
        Byte _byte = new Byte((byte) 1);
        Short _short = new Short((byte) 2);
        Integer _int = new Integer(3);
        Long _long = new Long(12l);
        Character _character = new Character('V');
        Boolean _boolean = new Boolean("true");
        Float _float = new Float(1.25F);
        Double _double = new Double(1.25D);

        System.out.println(_byte.byteValue());
        System.out.println(_byte.shortValue());
        System.out.println(_byte.intValue());
        System.out.println(_byte.longValue());
        System.out.println(_byte.floatValue());
        System.out.println(_byte.doubleValue());

        _short.shortValue();
        _int.intValue();
        _long.longValue();
        _character.charValue();
        _float.floatValue();
        _double.doubleValue();


        // parsePrimitive() is static method used to convert string to representing primitive
        // don't confuse Integer.valueOf("5"); its also static method but it return wrapper class object
        int i = Integer.parseInt("5");
        Boolean.parseBoolean("true");
        // character class doesn't contain corresponding parse method
    }

    public static void main(String[] args) {
        retrieve_instance();
        retieve_primitive();
    }

}
